from django.conf import settings
from django.db import models
# Create your models here.

def upload_invoice(instance, filename):
    return "registers/{user}/{filename}".format(user=instance.user, filename=filename)

class Register(models.Model):
    # User in system
    user            = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    # Register number of the invoice
    number          = models.IntegerField()
    # Invice description 
    description     = models.CharField(max_length=150)
    # Invoice image
    invoice_image   = models.ImageField(upload_to=upload_invoice, blank=True, null=True)
    # Updated invoice
    updated         = models.DateTimeField(auto_now=True)
    # Time stamp
    timestamp       = models.DateTimeField(auto_now_add=True)
    # Client name
    client          = models.CharField(max_length=150)
    # Register tax rate
    tax_rates       = models.DecimalField(max_digits=4, decimal_places=2)
    # Register total invoice
    total_invoice   = models.DecimalField(max_digits=20, decimal_places=2)
    # Register base
    base            = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return self.description or "" 