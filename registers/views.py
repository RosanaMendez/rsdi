import json
from purchasesAPI.mixins import JsonResponseMixin
from django.core.serializers import serialize
from django.views.generic import View
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from .models import Register

obj = Register.objects.all()

class SerilaizeDetailView(View):
    def get(self, request, *args, **kwargs):
        obj = Register.objects.get(id=2)
        """
        esta linea fue agregada luego
        """
        data = serialize("json", [obj,], fields=('user', 'number', 'client', 'description', 'tax_rates', 'total_invoice', 'base'))
        data = {
            "user": obj.user.username,
            "number": obj.number,
            "client": obj.client,
            "description": obj.description,
            "tax_rates": float(obj.tax_rates),
            "total_invoice": float(obj.total_invoice),
            "base": float(obj.base)
        } 
        json_data = json.dumps(data)
        return HttpResponse(json_data, content_type='application/json')

class SerilaizeListView(View):
    def get(self, request, *args, **kwargs):
        qs = Register.objects.all()
        data = serialize("json", qs, fields=('user', 'number', 'client', 'description', 'tax_rates', 'total_invoice', 'base'))
        json_data = data
        return HttpResponse(json_data, content_type='application/json')       
                    